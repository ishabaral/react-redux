import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { decrease, increment, signIn, signOut } from "../actions";

function CounterHooks() {
  const counter = useSelector((state) => state.counter);
  const isLogged = useSelector((state) => state.isLogged);
  const dispatch = useDispatch();
  return (
    <div>
      <h2>Counter: {counter}</h2>
      <button onClick={() => dispatch(increment(5))}>Increase Count</button>
      <button onClick={() => dispatch(decrease())}>Decrease Count</button>
      <h2>LogggedIn status: {isLogged ? "true" : "false"}</h2>
      {isLogged ? (
        <p>I am seeing the private information</p>
      ) : (
        <p>Please login first</p>
      )}
      <button onClick={() => dispatch(signIn())}>Sign In</button>
      <button onClick={() => dispatch(signOut())}>Sign Out</button>
    </div>
  );
}

export default CounterHooks;
