import React from "react";
import { connect } from "react-redux";
import { decrease, increment, signIn, signOut } from "../actions";

function Counter(props) {
  return (
    <div>
      <button onClick={props.incOrDecs}>IncOrDec</button>
      <h1>Count: {props.count}</h1>
      <button onClick={props.increment}>Increase Count</button>
      <button onClick={props.decrease}>Decrease Count</button>
      <h1>Logged in status: {props.isLogged ? "true" : "false"}</h1>
      {props.isLogged ? (
        <p style={{ color: "red" }}>I am seeing the private information</p>
      ) : (
        <p>Please login first</p>
      )}
      <button onClick={props.signIn}>Sign In</button>
      <button onClick={props.signOut}>Sign Out</button>
      <p>My name is {props.myName}</p>
    </div>
  );
}

const mapStateToProps = (state, ownProps) => {
  //ownProps refers to the props of the own/same component i.e props of Counter.js
  return {
    count: state.counter,
    isLogged: state.isLogged,
    myName: ownProps.name,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const count = ownProps.increase
    ? () => dispatch(increment(5))
    : () => dispatch(decrease());
  return {
    incOrDecs: count,
    increment: () => dispatch(increment(5)),
    decrease: () => dispatch(decrease()),
    signIn: () => dispatch(signIn()),
    signOut: () => dispatch(signOut()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Counter);
