import "./App.css";
import { useDispatch, useSelector } from "react-redux";
import { decrease, increment, signIn, signOut } from "./actions";
import Counter from "./components/Counter";
import CounterHooks from "./components/CounterHooks";

function App() {
  return (
    <div className="App">
      <CounterHooks />
      <Counter name="Isha" />
      <Counter increase />
    </div>
  );
}

export default App;
